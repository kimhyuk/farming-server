import { Strategy, ExtractJwt } from "passport-jwt";
import model from "../models";



const params = {
    secretOrKey: process.env.JWT_SECRET,
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
}

const strategy = new Strategy(params, (payload, done) => {
    model.UserModel.findById(payload._id)
    .then(async(user)=>{
        return done(null,user);
    })
    .catch((err)=>{
        return done(err);
    });
});

export default strategy;