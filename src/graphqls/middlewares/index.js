import jwt from "./jwt";

const middlewares = [jwt];

export default middlewares;