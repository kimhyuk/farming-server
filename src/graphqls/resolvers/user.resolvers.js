import models from "../../models"
import debug from "debug";
import validator from "validator";
import {ValidationError, UserInputError} from "apollo-server";
import jsonwebtoken from "jsonwebtoken";

const _debug = debug("gql","resolver")

export default {
    Query: {
        user: async(_, {id}) => {
            const user =  await models.UserModel.findById(id);
            return user;
        },
        users: async() => {
            const users = await models.UserModel.find();
            return users;
        }
    },
    Mutation: {
        signup: async (req,{email,username,password}) => {
            if(await models.UserModel.findByUserEmail(email)) {
                throw new ValidationError("중복된 이메일이 있습니다.");
            }
            else if(await models.UserModel.findByUsername(username)) {
                throw new ValidationError("중복된 사용자 이름입니다.");
            }

            const user = await models.UserModel.create({
                email,
                username,
                password
            });
            
            const token = jsonwebtoken.sign({
                _id: user._id,
                email: user.email,
                username: user.username
            }, process.env.JWT_SECRET, { expiresIn: '1y' })
    
            return new Promise((resolve)=>resolve(token)); 
        },
        login: async(_,{email,password},{req}) => {
            let user = undefined;
            if(validator.isEmail(email)){
                user = await models.UserModel.findByUserEmail(email);
            }
            else {
                user = await models.UserModel.findByUsername(email);
            }

            if(!user) {
                throw new ValidationError("이메일 혹은 이름이 올바르지 않습니다.");
            }

            if(!await user.verifyPassword(password)) {
                throw new ValidationError("비밀번호가 올바르지 않습니다.");
            }

            const token = jsonwebtoken.sign({
                _id: user._id,
                email: user.email,
                username: user.username
            }, process.env.JWT_SECRET, { expiresIn: '1y' })
    
            return new Promise((resolve)=>resolve(token)); 
        }
    }
}