import config from "../server.config";
import { GraphQLServer} from "graphql-yoga";
import path from "path";
import express from "express";
import graphqlProp from "./graphqls";
import db from "./utils/db";
import passport from 'passport'
import debug from "debug";
import passport_jwt from "./passport/jwt";

const _debug = debug("app");


const dbOptions = {
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD
}
const serverOptions = {
    port: process.env.PORT || 4000,
    endpoint: process.env.GRAPHQL_ENDPOINT,
    subscriptions: process.env.GRAPHQL_SUBSCRIPTIONS,
    playground: process.env.GRAPHQL_PLAYGROUND

}

const configPath = config.getPath();

db(dbOptions,{ useNewUrlParser: true, dbName:process.env.DB_DATABASE_NAME });

const server = new GraphQLServer({
    typeDefs: graphqlProp.typeDefs,
    resolvers: graphqlProp.resolvers,
    middlewares: graphqlProp.middlewares,
    context: ({ request }) => ({
        user: request.user  
    })
});

server.use(express.static(configPath.static));
server.use(express.static("/uploads",configPath.uploads))
passport.use(passport_jwt);
server.use(process.env.GRAPHQL_ENDPOINT, (req, res, next) => {
    passport.authenticate('jwt', { session: false }, (err, user, info) => {
        if (user) {
            req.user = user;
        }
        _debug(req.header);
        _debug(user);
        next();
    })(req, res, next)
  })

server.start(serverOptions, ()=> console.log('Server is running on localhost'+ serverOptions.port));
server.get("/",(req,res,next) => {
    // 해당 위치에 없을 경우 처리해야됨
    const indexHtml = path.join(configPath.static,"index.html");
    res.sendFile(indexHtml);
});

