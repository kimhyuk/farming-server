import debug from "debug";

const _debug = debug("middlewares");


const logInput = async (resolve, root, args, context, info) => {
    _debug(`1. logInput: ${JSON.stringify(args)}`)
    _debug(`2 context: ${JSON.stringify(context)}`);
    const result = await resolve(root, args, context, info)
    return result
}

export default logInput;