import mongoose from "mongoose";
import debug from "debug";

const _debug = debug("mongo");

mongoose.Promise = global.Promise;


export default async ({username,password,host,port},options) => {
    if(!host || !port) {
        _debug("please check host or port");
        return new Promise((_,reject)=>reject("please check host or port"));
    }

    return mongoose.connect(`mongodb://${username}:${password}@${host}:${port}/admin`,options).then(
        () => {
            _debug("connect success!");  
        },
        (err) => {
            _debug("connected fail!");
            _debug(err);
        }
    );
}